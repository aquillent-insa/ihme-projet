import base64
import io
import json
import os

import discord
import requests
from discord.ext import commands

TOKEN = os.getenv("TOKEN")
SERVER_URL = os.getenv("SERVER_URL")

bot = commands.Bot(command_prefix="!")


@bot.command()
async def process(ctx):
    if "attaque" in ctx.message.content:
        await ctx.message.channel.send("WOUF !  :dog:")
    else:
        try:
            im = await ctx.message.attachments[0].read()
            r = requests.post(SERVER_URL + "/upload", files={"the_file": im})
            if r:
                dct = json.loads(r.text)
                img = io.BytesIO(base64.b64decode(dct["image"]))
                mess = discord.File(img, "test.png")

                print("Image created !")
                await ctx.message.channel.send(
                    f"{dct['emotion']}  {dct['emoji']}", files=[mess]
                )
            else:
                raise Exception
        except Exception as e:
            await ctx.message.channel.send(
                "¡Ay ay ay ay! Quelque chose n'a pas marché :("
            )


print("Client running...")
print("Linked to", SERVER_URL)
bot.run(TOKEN)
