import os

import numpy as np
import torch
import torchvision.transforms as transforms
from PIL import Image
from skimage.transform import resize

from models.vgg import VGG


def rgb2gray(rgb):
    """Fonction qui convertit les image RGB en niveaux de gris
    """
    return np.dot(rgb[..., :3], [0.299, 0.587, 0.114])


class Classifier:
    def __init__(self):

        self.cut_size = 44
        self.transform_test = transforms.Compose(
            [
                transforms.TenCrop(self.cut_size),
                transforms.Lambda(
                    lambda crops: torch.stack(
                        [transforms.ToTensor()(crop) for crop in crops]
                    )
                ),
            ]
        )

    def get_emotion(self, raw_img):
        gray = rgb2gray(raw_img)  # Conversion en niveaux de gris
        gray = resize(gray, (48, 48), mode="symmetric").astype(
            np.uint8
        )  # Redimensionnement de l'image (le VGG pré-appris n'accepte que des images de taille 48x48)

        img = gray[
            :, :, np.newaxis
        ]  # VGG n'accepte que les images RGB => on duplique l'image N&B sur 3 canaux pour simuler le RGB

        img = np.concatenate((img, img, img), axis=2)
        img = Image.fromarray(img)  # Image au sens du module Pillow
        inputs = self.transform_test(
            img
        )  # L'image est découpée en 10 régions distinctes

        class_names = [
            "Colère",
            "Dégoût",
            "Peur",
            "Joie",
            "Tristesse",
            "Surprise",
            "Neutre",
        ]  # Liste des différentes prédictions possibles

        net = VGG(
            "VGG19"
        )  # On instancie notre réseau de neurones suivant le modèle VGG19
        checkpoint = torch.load(
            os.path.join(os.path.dirname(__file__), "models/model.t7"),
            map_location=torch.device("cpu"),
        )  # On charge le modèle pré-appris
        net.load_state_dict(
            checkpoint["net"]
        )  # On incorpore le modèle pré-appris à notre VGG19
        net.cpu()  # On bascule le modèle sur le processeur
        net.eval()  # On passe le modèle en mode évaluation (inférence) : les couches Dropout (entre autres) sont désactivées

        with torch.no_grad():  # Tout en ne calculant pas la rétropropagation du gradient
            ncrops, c, h, w = np.shape(
                inputs
            )  # On récupère la taille de l'image en entrée

            inputs = inputs.view(
                -1, c, h, w
            )  # On la redimensionne suivant le format compris par PyTorch
            inputs = (
                inputs.cpu()
            )  # On transforme l'image en un tenseur sur le processeur

            outputs = net(inputs)  # On effectue l'inférence
            outputs_avg = outputs.view(ncrops, -1).mean(
                0
            )  # On moyenne les valeurs pour chaque canal de l'image

            _, predicted = torch.max(
                outputs_avg.data, 0
            )  # L'émotion de score maximal est celle prédite

        return class_names[predicted]
