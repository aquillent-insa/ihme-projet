import os

import cv2


class FaceCropper:
    def __init__(self):
        self.face_cascade = cv2.CascadeClassifier(
            cv2.data.haarcascades
            + "haarcascade_frontalface_default.xml"  # Chemin d'accès au fichier décrivant la cascade
        )

    def generate(self, img):
        """Fonction qui renvoie une image centrée sur le visage détecté
        """
        if img is None:
            raise Exception("Fichier invalide !")

        faces_coords = self.face_cascade.detectMultiScale(
            img
        )  # Méthode de Viola et Jones
        if faces_coords is None:
            raise Exception("Visage non détecté !")

        for (x, y, w, h,) in faces_coords:
            # Pour chacune des coordonnées du visage trouvé, x et y étant les coordonnées du point en bas à gauche
            # La bounding box peut être rectangulaire, donc avec w =/= h, or on souhaite une image carrée
            r = max(w, h) / 2  # Rayon autour du visage
            centerx = x + w / 2  # Coordonnée x du centre du visage
            centery = y + h / 2  # Coordonnée y du centre du visage
            nx = int(centerx - r)  # Nouvelle coordonnée x du point en bas à gauche
            ny = int(centery - r)  # Nouvelle coordonnée y du point en bas à gauche
            nr = int(r * 2)

            faceimg = img[ny : ny + nr, nx : nx + nr]
            lastimg = cv2.resize(faceimg, (256, 256))
            return lastimg
