import base64
import io
import time
import traceback

import numpy as np
from flask import Flask, request
from PIL import Image

from face_cropper import FaceCropper
from predict import Classifier

app = Flask(__name__)

log = {}

emojis = {
    "Colère": ":rage:",
    "Dégoût": ":sick:",
    "Peur": ":scream:",
    "Joie": ":grinning:",
    "Tristesse": ":cry:",
    "Surprise": ":astonished:",
    "Neutre": ":neutral_face:",
}


@app.route("/", methods=["GET"])
def hello():
    return "Hello world !"


@app.route("/upload", methods=["POST"])
def upload():
    im_io = request.files["the_file"]
    image = Image.open(im_io)
    img = np.array(image)
    try:
        fc = FaceCropper()
        image_fc = fc.generate(img)
        clf = Classifier()
        pred = clf.get_emotion(image_fc)
        byte = io.BytesIO()
        im_fin = Image.fromarray(image_fc)
        im_fin.save(byte, format="PNG")
        byte.seek(0)
        enc = base64.b64encode(byte.read())
        fi = {"image": enc.decode("ascii"), "emotion": pred, "emoji": emojis[pred]}
        return fi, 200
    except Exception as e:
        log[time.ctime()] = str(e).strip()
        traceback.print_exc()
        return "", 500


@app.route("/debug", methods=["GET"])
def debug():
    return log


if __name__ == "__main__":
    app.run()
