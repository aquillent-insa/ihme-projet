# Manuel d'installation et d'utilisation du système

## Installation de Discord

Discord est un logiciel cherchant à fusionner Skype, Mumble et TeamSpeak. A la base conçu pour des joueurs, l'outil devient utile pour les développeurs. La possibilité de créer des bots et de les activer sur des serveurs communautaires est ce qui nous intéresse ici.
Pour l'utiliser, il est nécessaire de créer un compte Discord en suivant les étapes à partir du [site officiel](https://discordapp.com/).

Une fois le compte créé, deux possibilités s'offrent à vous :
  *  Utiliser Discord sur Internet. Dans ce cas, il suffit de cliquer sur "Ouvrir Discord" sur [cette page](https://discordapp.com/).
  *  Utiliser Discord à l'aide du logiciel téléchargeable en suivant [ce lien](https://discordapp.com/download).

Une fois connecté à votre compte, cliquez sur [ce lien](https://discord.gg/bxtzrQu) et vous voilà invité dans le serveur Discord où se trouve notre bot FERnando.

## Installation de Docker

[Docker - Wiki Ubuntu FR](https://doc.ubuntu-fr.org/docker)

Si vous ne souhaitez pas utiliser Docker, rendez-vous à la section **Utilisation sans Docker**.

## Compilation des images

### Bot

Aller dans le dossier `bot`, puis lancer cette commande (attention au point à la fin de la commande !) :

```console
$ docker build -t ihme-bot .
```

### Serveur

Aller dans le dossier `server`, puis :

```console
$ docker build -t ihme-server .
```

> :warning: La compilation de l'image peut durer longtemps, en fonction de la puissance de votre ordinateur et la vitesse de votre connexion internet : l'image finale pèse environ 3Gb (PyTorch prend énormément de place, mais il faut malheureusement l'installer en entier pour bénéficier de ses fonctions d'inférence)

### Lancement des images

#### Pour le bot :

Remplacer CHEMIN_ENV par le chemin d'accès au fichier `.env` (`--env-file=../.env` si vous êtes dans le dossier `bot`)

```console
$ docker run --env-file=CHEMIN_ENV --network=host ihme-bot
```
#### Pour le serveur :

Depuis n'importe où :

```console
$ docker run --network=host ihme-server
```

## Utilisation sans Docker

### Installation de Pipenv

Pipenv est un gestionnaire de dépendances qui permet de créer facilement des environnements virtuels.

```console
$ pip install --user pipenv
$ pipenv install --skip-lock
```

L'installation des dépendances peut durer un certain temps, qui dépend grandement de votre connexion internet.

Pour lancer le bot et le serveur, dans deux terminaux différents :

```console
$ cd bot
$ pipenv run python3 bot.py
```

> :information_source: À noter qu'en utilisant Pipenv, le fichier `.env` est automatiquement chargé.

```console
$ cd server
$ pipenv run python3 server.py
```

## Commandes du bot

### Détection d'émotion

Tapez la commande `!process` suivi d'une image d'un visage humain vu de face pour que FERnando retourne l'émoji et l'émotion correspondante.

### Attaque

Tapez la commande `!process attaque` pour que FERnando aboie son _Hello world_ avec véhémence.